const { Pool } = require('pg');
const config = require('../config');

const pool = new Pool(config().db);


async function check(id) {
    console.log('chequeando id: ' + id);
    const sql1 = ' \
        select pd.leer, pd.escribir, pd.eliminar, count(*) \
        from sudocu.usuarios u \
        left join sudocu.usuarios_permisos_documentos pd on pd.id_usuario = u.id \
        left join sudocu.documentos_tipos dt on dt.id = pd.id_tipo_documento \
        where dt.nombre = \'expediente\' and u.id = $1 \
        group by 1,2,3; \
        ';
    const response = await pool.query(sql1, [id]);
    console.log('query1 ok');
    const c = response.rows.length>0 && response.rows[0].count == 1;
    const expe = c?{
        expedientes: response.rows[0].leer || response.rows[0].escribir || response.rows[0].eliminar,
        leer: response.rows[0].leer,
        escribir: response.rows[0].escribir,
        eliminar: response.rows[0].eliminar
    }:{
        expedientes: false,
        leer: false,
        escribir: false,
        eliminar: false
    };
    console.log('expe: ' + JSON.stringify(expe));

    const sql2 = ' \
        select \
          u.apellido as apellido, \
          u.nombre as usuario, \
          u.email, \
          coalesce(pp.nombre, \'diferente\') as perfil \
        from sudocu.usuarios u \
        left join sudocu.usuarios_permisos up on up.id_usuario = u.id \
        left join unaj.pp pp on up.permisos = pp.permisos \
        where u.id = $1 \
        ;';
    const response2 = await pool.query(sql2, [id]);
    console.log('query2 ok');
    console.log(response2.rows[0]);
    if(response2.rows.length != 1) {
        console.log('Error en query 2');
        //throw new Error('Error en query 2');
        return {
            normal: false,
            firma: false,
            expedientes: false
        };
    }
    const perfil = response2.rows[0].perfil;
    const sql3 = 'select u.nivel_firma from sudocu.usuarios u where u.id = $1';
    const response3 = await pool.query(sql3, [id]);
    const nfo = response3.rows[0].nivel_firma;

    let rn = true;
    let rf;
    let re;
    if(response.rows.length > 0 && response.rows[0].count > 1) {
    console.log('caso 1');
        rn = false;
    }
    if(perfil !== 'basico' && perfil !== 'nosi' && perfil !=='sino' && perfil !=='full') {
    console.log('caso 2');
        rn = false;
    }
    if(expe.expedientes && expe.leer && expe.escribir && expe.eliminar) {
        re = true;
    } else if(! expe.expedientes) {
        re = false;
    } else {
        re = true;
    console.log('caso 3');
        rn = false;
    }
    if(re && (perfil==='basico' || perfil==='sino')) {
    console.log('caso 4');
        rn = false;
    }
    let nf;
    if(nfo && nfo.reconocida) {
    console.log('caso 5');
        rn = false;
    }
    nf = nfo && nfo.basica === true;
    rf = perfil==='sino' || perfil === 'full';
    if(nf !== rf) {
    console.log('caso 6');
        rn = false;
    }
console.log(rn, rf, re);
    return {
        normal: rn,
        firma: rf,
        expedientes: re
    };
}

async function completar(row) {
    let permisos;
    permisos = await check(row.id).catch((e)=>{throw e});
    return {
        id: row.id,
        apellido: row.apellido,
        nombre: row.nombre,
        email: row.email,
        normal: permisos.normal,
        firma: permisos.firma,
        expedientes: permisos.expedientes
    };
}

async function getUsuarios() {
    const sql = 'select id, apellido, nombre, email from sudocu.usuarios order by apellido, nombre';
    const response = await pool.query(sql);
    console.log('query ok');
    const prom = [];
    for(row of response.rows) {
        prom.push(completar(row));
    }
    usuarios = [];
    for(p of prom) {
        p.catch((e)=>{
            console.log('un usuario no mostrado');
        });
        usuarios.push(await p);
    }
    return usuarios;
}

async function getUsuario(id) {
    const sql = 'select id, nombre, apellido, email from sudocu.usuarios where id = $1';
    const response = await pool.query(sql, [id]);
    console.log('query ok');
    return completar(await response.rows[0]);
}

async function setPerfil(id, perfil) {
    if(!config().permitir_modificacion) {
      return false;
    }
    const client = await pool.connect();
    client.query('begin');
    try {
        await client.query('delete from sudocu.usuarios_permisos where id_usuario = $1', [id]);
        await client.query('delete from sudocu.usuarios_permisos_documentos where id_usuario = $1;', [id]);
        await client.query('insert into sudocu.usuarios_permisos(id_usuario, permisos) values ($1, (select permisos from unaj.pp where nombre = $2))', [id, perfil]);
        await client.query('insert into sudocu.usuarios_permisos_documentos(id_usuario, id_tipo_documento, leer, escribir, eliminar) values ($1, (select id from sudocu.documentos_tipos where nombre = \'nota\'), 1, 1, 1)', [id]);
        if(perfil === 'full' || perfil === 'nosi') {
            await client.query('insert into sudocu.usuarios_permisos_documentos(id_usuario, id_tipo_documento, leer, escribir, eliminar) values ($1, (select id from sudocu.documentos_tipos where nombre = \'expediente\'), 1, 1, 1)', [id]);
        }
        if(perfil === 'full' || perfil === 'sino') {
            await client.query('update sudocu.usuarios set nivel_firma = \'{"basica": true}\'::jsonb where id = $1', [id]);
        } else {
            await client.query('update sudocu.usuarios set nivel_firma = \'{"basica": false}\'::jsonb where id = $1', [id]);
        }
        await client.query('commit');
    } catch(e) {
        console.log(e);
        await client.query('rollback');
    } finally {
        client.release();
    }
    return true;
}
async function blanquear(id) {
    return setPerfil(id, 'basico');
}
async function quitarfirma(id) {
    const previo = await check(id);
    const perfil = previo.expedientes? 'nosi': 'basico';
    return setPerfil(id, perfil);
}
async function darfirma(id) {
    const previo = await check(id);
    const perfil = previo.expedientes? 'full': 'sino';
    return setPerfil(id, perfil);
}
async function darexpedientes(id) {
    const previo = await check(id);
    const perfil = previo.firma? 'full': 'nosi';
    return setPerfil(id, perfil);
}
async function quitarexpedientes(id) {
    const previo = await check(id);
    const perfil = previo.firma? 'sino': 'basico';
    return setPerfil(id, perfil);
}

module.exports = {
    getUsuarios,
    getUsuario,
    blanquear,
    darfirma, quitarfirma,
    darexpedientes, quitarexpedientes
};

