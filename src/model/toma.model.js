const { Pool } = require('pg');
const config = require('../config');

const pool = new Pool(config().db);


async function estado() {
    console.log('chequeando');
    const sql1 = ' \
        select ts, admin, mail, usuario_idp \
        from unaj.toma_cuenta \
        where devuelta = false; \
        ';
    const response1 = await pool.query(sql1);
    console.log('query1 ok');
    const b1 = response1.rows.length == 1;
    const sql2 = ' \
        select u.apellido, u.nombre, u.email \
        from sudocu.usuarios u \
        inner join sudocu.usuarios_cuentas c \
        on c.id_usuario_sudocu = u.id \
        where c.email = \'sudocumantenimiento@unaj.edu.ar\'; \
        ';
    const response2 = await pool.query(sql2);
    console.log('query2 ok');
    const b2 = response2.rows.length == 1;
    const r = { };
    if(b1 != b2) {
        r.tomado = 'inconsistente';
    } else {
        r.tomado = b1 && b2;
        if(r.tomado) {
            if(response1.rows[0].mail == response2.rows[0].email) {
                r.ok = true;
                r.apellido = response2.rows[0].apellido;
                r.nombre = response2.rows[0].nombre;
                r.email = response2.rows[0].email;
                r.admin = response1.rows[0].admin;
                r.usuario = response1.rows[0].usuario_idp;
            } else {
                r.ok = false;
            }
        }
    }
    console.log(r);
    return r;
}

async function devolver() {
    if(!config().permitir_modificacion) {
      return false;
    }
    const e = await estado();
    const sql1 = ' \
        update sudocu.usuarios_cuentas \
        set id_usuario_idp = $1, \
            email = $2 \
        where id = ( \
          select c.id \
          from sudocu.usuarios u \
          inner join sudocu.usuarios_cuentas c \
          on c.id_usuario_sudocu = u.id \
          where u.email = $2 \
          and c.email = \'sudocumantenimiento@unaj.edu.ar\' \
        ); \
        ';
    const sql2 = '\
        update unaj.toma_cuenta \
        set devuelta = true \
        where ts =( \
          select ts from unaj.toma_cuenta \
          where devuelta = false); \
        ';

    if(e.tomado === true && e.ok === true) {
        const client = await pool.connect();
        client.query('begin');
        try {
            await client.query(sql1, [e.usuario, e.email]);
            await client.query(sql2);
            await client.query('commit');
        } catch(e) {
            console.log(e);
            await client.query('rollback');
        } finally {
            client.release();
        }
    } else {
        throw 'No se puede';
    }
    return true;
}

async function existe(cuenta) {
    const email = cuenta + '@unaj.edu.ar';
    const sql0 = 'select id from sudocu.usuarios where email = $1;';
    const client = await pool.connect();
    let r0 = null;
    try {
        r0 = await client.query(sql0, [email]);
    } catch(e) {
        console.log(e);
        return 0;
    } finally {
        client.release();
    }
    return r0.rows.length === 1;
}

async function tomar(quien, cuenta) {
    const email = cuenta + '@unaj.edu.ar';
    if(!config().permitir_modificacion) {
      return false;
    }
    const e = await estado();
    const sql1 = ' \
        insert into unaj.toma_cuenta( \
          ts, admin, mail, usuario_idp) \
        values( \
          now(), $1, $3, $2); \
        ';
    const sql2 = ' \
        update sudocu.usuarios_cuentas \
        set id_usuario_idp = \'sudocumantenimiento\', \
            email = \'sudocumantenimiento@unaj.edu.ar\' \
        where id = ( \
          select c.id \
          from sudocu.usuarios u \
          inner join sudocu.usuarios_cuentas c \
          on c.id_usuario_sudocu = u.id \
          where u.email = $1 \
        ); \
        ';

    if(e.tomado === false) {
        if(quien==='' || quien===null || !quien) {
            return 'Debe seleccionar quién toma';
        }
        const client = await pool.connect();
        client.query('begin');
        if((await existe(cuenta))!==true) {
            return 'No existe usuario ' + cuenta;
        }
        try {
            await client.query(sql1, [quien, cuenta, email]);
            await client.query(sql2, [email]);
            await client.query('commit');
        } catch(e) {
            console.log(e);
            await client.query('rollback');
            return false;
        } finally {
            client.release();
        }
    } else {
        throw 'No se pudo';
    }
    return true;
}

module.exports = {
    estado,
    devolver,
    tomar
};

