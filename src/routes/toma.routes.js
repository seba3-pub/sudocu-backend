const {Router} = require('express');
const router = Router();

const config = require('../config.js');
const {estado_toma, devolver, tomar_cuenta} =
    require('../controllers/toma.controller');

function p(u) {
    const r = `${config().uri_prefijo}/${u}`;
    console.log('ruta: ' + r);
    return r;
}

router.get(p('estado_toma'), estado_toma);
router.get(p('devolver'), devolver);
router.post(p('tomar_cuenta'), tomar_cuenta);

module.exports = router;

