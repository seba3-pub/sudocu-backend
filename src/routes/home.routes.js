const {Router} = require('express');
const router = Router();

const config = require('../config.js');
const {home} =
    require('../controllers/home.controller');

function p(u) {
    const r = `${config().uri_prefijo}/${u}`;
    console.log('ruta: ' + r);
    return r;
}

router.get(p('home'), home);

module.exports = router;

