const {Router} = require('express');
const router = Router();

const config = require('../config.js');
const {getUsuarios, getUsuario, blanquear, darfirma, quitarfirma, darexpedientes, quitarexpedientes} =
    require('../controllers/permisos.controller');

function p(u) {
    const r = `${config().uri_prefijo}/${u}`;
    console.log('ruta: ' + r);
    return r;
}

router.get(p('usuarios'), getUsuarios);
console.log(p('usuarios'));
router.get(p('usuario/:id'), getUsuario);
router.get(p('blanquear/:id'), blanquear);
router.get(p('darfirma/:id'), darfirma);
router.get(p('quitarfirma/:id'), quitarfirma);
router.get(p('darexpedientes/:id'), darexpedientes);
router.get(p('quitarexpedientes/:id'), quitarexpedientes);

module.exports = router;

