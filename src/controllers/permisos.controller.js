const config = require('../config');
const model = require('../model/permisos.model.js');
const ejs = require('ejs');

function format(usuarios) {
    return usuarios;
}

function render(res, usuarios) {
    res.status(200).render('permisos', {
        usuarios: usuarios,
        prefijo: config().uri_prefijo,
        config: config
    });
}

async function getUsuarios(req, res) {
    const um = await model.getUsuarios().catch((e)=>
        {
            console.log('error' + e);
            res.status(500).send(e);
        });
    const usuarios = format(um);
    render(res, usuarios);
}

async function getUsuario(req, res) {
    const id = req.params.id;
    const usuario = format([await model.getUsuario(id)])[0];
    render(res, [usuario]);
}

async function blanquear(req, res) {
    if(!config().permitir_modificacion) {
        res.status(401).send('No están habilitadas las modificaciones');
        return;
    }
    const id = req.params.id;
    const r = await model.blanquear(id);
    if(r) {
        const usuario = format([await model.getUsuario(id)])[0];
        render(res, [usuario]);
    } else {
        res.status(500).send('Error!');
    }
}

async function darfirma(req, res) {
    if(!config().permitir_modificacion) {
        res.status(401).send('No están habilitadas las modificaciones');
        return;
    }
    const id = req.params.id;
    const r = await model.darfirma(id);
    if(r) {
        const usuario = format([await model.getUsuario(id)])[0];
        render(res, [usuario]);
    } else {
        res.status(500).send('Error!');
    }
}

async function quitarfirma(req, res) {
    if(!config().permitir_modificacion) {
        res.status(401).send('No están habilitadas las modificaciones');
        return;
    }
    const id = req.params.id;
    const r = await model.quitarfirma(id);
    if(r) {
        const usuario = format([await model.getUsuario(id)])[0];
        render(res, [usuario]);
    } else {
        res.status(500).send('Error!');
    }
}

async function darexpedientes(req, res) {
    if(!config().permitir_modificacion) {
        res.status(401).send('No están habilitadas las modificaciones');
        return;
    }
    const id = req.params.id;
    const r = await model.darexpedientes(id);
    if(r) {
        const usuario = format([await model.getUsuario(id)])[0];
        render(res, [usuario]);
    } else {
        res.status(500).send('Error!');
    }
}

async function quitarexpedientes(req, res) {
    if(!config().permitir_modificacion) {
        res.status(401).send('No están habilitadas las modificaciones');
        return;
    }
    const id = req.params.id;
    const r = await model.quitarexpedientes(id);
    if(r) {
        const usuario = format([await model.getUsuario(id)])[0];
        render(res, [usuario]);
    } else {
        res.status(500).send('Error!');
    }
}

module.exports = {
    getUsuarios,
    getUsuario,
    blanquear,
    darfirma, quitarfirma,
    darexpedientes, quitarexpedientes
};

