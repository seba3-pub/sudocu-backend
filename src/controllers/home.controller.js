const config = require('../config');
const ejs = require('ejs');

async function home(req, res) {
  res.status(200).render('home');
}

module.exports = {
    home
};

