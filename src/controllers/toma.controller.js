const config = require('../config');
const model = require('../model/toma.model.js');
const ejs = require('ejs');

function render(res, toma) {
    res.status(200).render('toma', {
        toma: toma,
        prefijo: config().uri_prefijo,
        config: config
    });
}

async function estado_toma(req, res) {
  //const id = req.params.id;
  const t = await model.estado().catch((e)=>
    {
      console.log('error' + e);
      res.status(500).send(e);
    });
  render(res, t);
}

async function devolver(req, res) {
  const t = await model.devolver().catch((e)=>
    {
      console.log('error' + e);
      res.status(500).send(e);
    });
  if(t) {
    return await estado_toma(req, res);
  } else {
    return res.status(500).send(t);
  }
  render(res, t);
}

async function tomar_cuenta(req, res) {
  const cuenta = req.body.cuenta_tomar;
  const quien = req.body.quien_toma;
  const t = await model.tomar(quien, cuenta).catch((e)=>
    {
      console.log('error' + e);
      res.status(500).send(e);
    });
  if(t) {
    if(t !== true) {
        return res.status(500).send(t);
    }
    return await estado_toma(req, res);
  } else {
    return res.status(500).send(t);
  }
  render(res, t);
}

module.exports = {
    estado_toma,
    devolver,
    tomar_cuenta
};

