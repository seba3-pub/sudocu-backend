const express = require('express');
const app = express();
const config = require('./config.js');

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(config().uri_prefijo + '/static', express.static('static'));
app.set('view engine', 'ejs');

app.use(require('./routes/home.routes.js'));
app.use(require('./routes/permisos.routes.js'));
app.use(require('./routes/toma.routes.js'));

console.log('Permitir modificacion: ' + config().permitir_modificacion);
console.log('DB host: ' + config().db.host);
console.log('Iniciando');
app.listen(3000);

