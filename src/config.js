const _config = [];
function config() {
    const e = process.env;
    if(!_config.ok) {
      _config.db = {
        host: e.DB_HOST,
        user: e.DB_USER,
        password: e.DB_PASSWORD,
        database: e.DB_NAME,
        port: e.DB_PORT
      }
      _config.permitir_modificacion = e.PERMITIR_MODIFICACION === "true";
      _config.uri_prefijo = e.URI_PREFIJO? e.URI_PREFIJO:'';
      _config.ok = true;
    }
    return _config;
}

module.exports = config;

